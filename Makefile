#*******************************************************************************
#
#      filename:  Makefile
#
#   description:  Make file to build the desired flip executable
#
#        author:  Hanuscin, Timothy
#      login id:  FA_14_CPS444_11
#
#         class:  CPS 444
#    instructor:  Perugini
#    assignment:  Homework #5
#
#      assigned:  September 21, 2017
#           due:  September 28, 2017
#
#******************************************************************************/

CC=gcc
OPTS=-c
OBJECTS=$(addsuffix .o,$(SRC)) #.o files
FINAL=LogExec
CS=$(wildcard *.c) #.c files
SRC=$(patsubst %.c,%,$(CS)) #.c files with .c

all: $(FINAL)

$(FINAL): $(OBJECTS)
	$(CC) -o $(FINAL) $(OBJECTS)

$(OBJECTS): %.o: %.c
	$(CC) $(OPTS) $< -o $@
	$(CC) -MM $< > $*.d

-include $(addsuffix .d,$(SRC))

clean: 
	@-rm *.o *.d $(FINAL)
